# Bookdemo

Basic crud app written with python flask. This demo now also includes basic
CI/CD concepts missing from previous iterations of BuzzCrate. The goal, clone
me and get started on your own kubernetes adventure with containers more 
entertaining than showing "hello world" in Apache or Nginx (still important).


# Prequesites

To use this EXACT demo, Arch Linux or Arch Linux ARM MUST be used for the 
builders. There is no reason why this project could be cloned and have 
images built without some changes to the build process. 

## Arch Linux Prequisites 

* Package installation minimum:
  * `pacman -S base-devel python buildah docker`
  * GitLab runner installed on system 



