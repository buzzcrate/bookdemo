"""Basic flask app to be made kubernetes native"""
import os

from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from psycopg2 import connect 
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from flask_sqlalchemy import SQLAlchemy

# If you view env in the container, config maps and secrets are saved as 
# environment variables. 
POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
POSTGRES_SERVICE_HOST = os.environ.get('POSTGRES_SERVICE_HOST')
POSTGRES_SERVICE_PORT = os.environ.get('POSTGRES_SERVICE_PORT')
# Create database connection string. 
database_file = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVICE_HOST}:{POSTGRES_SERVICE_PORT}/book'


def create_db():
	"""Create database quick and dirty, ignore error of database existing already."""
	con = connect(dbname='postgres', user=POSTGRES_USER, host=POSTGRES_SERVICE_HOST, password=POSTGRES_PASSWORD)
	con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
	cur = con.cursor()
	try:
		cur.execute('CREATE DATABASE book')
	except:
		pass
	cur.close()
	con.close()

create_db()

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file

db = SQLAlchemy(app)

class Book(db.Model):
	"""Book database model class"""
	__tablename__ = "book"
	title = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)

	def __repr__(self):
		"""Reprint override to give the title of the book."""
		return "<Title: {}>".format(self.title)

# Call after the database models have been defined. 
db.create_all()

@app.route("/", methods=["GET", "POST"])
def home():
	if request.form:
		try:
			book = Book(title=request.form.get("title"))
			db.session.add(book)
			db.session.commit()
		except Exception as e:
			print("Failed to add book") 
			print(e)
	books = Book.query.all()
	return render_template("home.html", books=books)

@app.route("/update", methods=["POST"])
def update():
	"""Update function on the title"""
	try:
		newtitle = request.form.get("newtitle")
		oldtitle = request.form.get("oldtitle")
		book = Book.query.filter_by(title=oldtitle).first()
		book.title = newtitle
		db.session.commit()
	except SQLAlchemy.exc.InvalidRequestError as e:
		print(e)
		print("Invalid Request, usually trying to add an existing book.")
	except InvalidRequestError as e:
		print(e)
		print("Invalid Request, usually trying to add an existing book.")
	except Exception as e:
		print("Couldn't update book title") 
		print(e)
	finally:
		pass
	return redirect("/")

@app.route("/delete", methods=["POST"])
def delete():
	title = request.form.get("title")
	book = Book.query.filter_by(title=title).first()
	db.session.delete(book)
	db.session.commit()
	return redirect("/")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='6550')
    #app.run(debug=True)
